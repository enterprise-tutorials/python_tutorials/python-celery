SHELL := /bin/bash
PROJECT_NAME := python-celery
HOST := localhost
PROJECT_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
PIPENV := PIPENV_VENV_IN_PROJECT=1 TMPDIR=$(PROJECT_DIR)/tmp pipenv --bare
IMAGES := $(PROJECT_NAME)
VENV_NAME?=venv
PYTHON=${VENV_NAME}/bin/python3

help:
	@echo "Development: "
	@echo "  setup-env          Sets up all pre-requisites for running this application"
	@echo "  venv_dev           Generate new Pipefile.lock and prepare virtual environment"
	@echo "  venv               Prepare virtual environment"
	@echo "  docs               Build pinto documentation"
	@echo "  test               Execute unit/integration tests"
	@echo "  run                Run development server"

	@echo "Build:"
	@echo "  docker-run         Build and run app via Docker"
	@echo "  clean              Clean build deliverables"

setup-env:
	sudo apt-get install python3 pip
	pip install pipenv
	pipenv install

clean:
	rm -rf build

venv:
	$(PIPENV) sync

venv_dev: Pipfile.lock
	$(PIPENV) sync --dev

docker-run:
	docker build \
	  --file=./Dockerfile \
	  --tag=$(PROJECT_NAME) ./
	docker run \
	  --detach=false \
	  --name=$(PROJECT_NAME) \
	  --publish=$(HOST):8080 \
      $(PROJECT_NAME)


test: venv
	${PYTHON} -m pytest

lint: venv
	${PYTHON} -m pylint
	${PYTHON} -m mypy

run:
	${PYTHON} manage.py runserver

docs:
	cd docs && make clean && make html
