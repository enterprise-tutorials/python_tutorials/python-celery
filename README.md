# python-celery
Example Python project using Celery framework.

For enterprise applications, it is always critical to offload some of the heavier or
long running tasks to a different thread instead of running on the main thread. Main
threads are typically kept for primary interactions with the service, so it is critical
to keep tasks short-running.

Celery requires a solution to send and receive messages; usually this comes in the
form of a separate service called a message broker. In this example, I have decided
to use [RabbitMQ](https://www.rabbitmq.com).

## Pre-requisites
* Understanding of Python
* Python 3.x environment installed
* Pipenv

## Building and Testing
* Clone the repository
```bash
git clone git@gitlab.com:enterprise-tutorials/python_tutorials/python-celery.git
```

* (Optional) If you do not have items mentioned in the Pre-requisites above, then execute
```bash
make setup-env
```

* Start RabbitMQ Docker in a new Terminal window
```bash
docker run -d -p 5672:5672 rabbitmq
```
If successfully started, you will see `Starting rabbitmq-server: SUCCESS.`

## Running the application

* Running the Celery worker server
```bash
pipenv run celery -A tasks worker --loglevel=info
```

## References
[Celery](https://pypi.org/project/celery/)

## Author
[mskariah1942](mskariah1942@gmail.com)
